var express = require('express'),
   app = express(),
   port = process.env.PORT || 3000;

var path = require('path');
var requestjson = require('request-json');

var db= null, collection = null;
var mongodb = require('mongodb');
var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
    assert = require('assert');

    MongoClient.connect('mongodb://admin123:admin123@ds163757.mlab.com:63757/fgutierrez', (err,client) => {
      try {
        if(err)
        {
          return console.log("No conecto" + err);
        }
        else {
            console.log("CONEXION BD: OK");
            db = client.db('fgutierrez');
       }
      } catch(err) {
              console.log(err.message)
      }
    });

var urlRaizMlab = "https://api.mlab.com/api/1/databases/fgutierrez/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var ClienteMLabRaiz;

var urlClientes = "https://api.mlab.com/api/1/databases/fgutierrez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlProductos = "https://api.mlab.com/api/1/databases/fgutierrez/collections/Productos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var bodyParser = require('body-parser');
var movimientosJSON= require('./movimientosv2.json');

app.use(bodyParser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");
  next();
});



app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get ('/', function(req,res){
 res.send('Servidor Funcionando backend API');
})

/*app.get ('/', function(req,res){
  res.sendFile(path.join(__dirname, 'index.html'));
})*/

app.get ('/Clientes/:idcliente', function(req,res){
  res.send('Aqui tienes al cliente número: ' + req.params.idcliente);
})

app.post ('/', function(req,res){
  res.send('Hemos recibido su petición post');
})
app.put ('/', function(req,res){
  res.send('Hemos recibido su petición cambiada');
})
app.delete ('/', function(req,res){
  res.send('Petición para borrar');
})

app.get ('/v1/movimientos', function(req,res){
  res.sendfile('movimientos v1.json');
})

app.get ('/v2/movimientos', function(req,res){
  res.json(movimientosJSON);
})

app.get ('/v2/movimientos/:indice', function(req,res){
  console.log(req.parms.indice);
  res.json(movimientosJSON[req.params.indice]);
})

app.get ('/v3/movimientosquery', function(req,res){
  console.log(req.query);
  res.send('Recibido');
})
app.post ('/v3/movimientos', function(req,res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo);
  res.send('Movimiento dado de alta')
})

app.get ('/Clientes', function(req,res){
  var clienteMlab = requestjson.createClient(urlClientes);
  clienteMlab.get('',function(err, resM, body){
    if (err){
      console.log(body);
    }else {
      res.send(body);
    }
  })
})

/*Recupera Usuario Activo*/
app.get ('/Usuarios/:email', function(req,res){
  //console.log(req.params.email);
  db.collection('Clientes').find({email:req.params.email},{nombre:1,apellido:1}).toArray(function(err, body) {
    if(err){
      console.log("Sin acceso a BD.");
      return res.status(400).send("Sin acceso a tabla de clientes");
    } else{
    //    console.log(body);
      return  res.send(body);
    }
  })
})


/* REcupera los datos de los productos desde Momgo*/
app.get ('/Productos', function(req,res){
  var productoMlab = requestjson.createClient(urlProductos);
  productoMlab.get('',function(err, resM, body){
    if (err){
      console.log(body);
    }else {
      res.send(body);
    }
  })
})

/* REcupera los datos de los productos desde Momgo por Producto*/
app.get ('/Productos/:idproducto', function(req,res){
  var urlProducto = urlProductos + '&q={"idProducto":"' + req.params.idproducto + '"}';
  //res.send(urlProducto);
  var productoMlab = requestjson.createClient(urlProducto);
  productoMlab.get('',function(err, resM, body){
    if (err){
      console.log(body);
    }else {
      res.send(body);
    }
  })
})

/* Valida el usuario en base a los parametros que envian desde app-login.html*/
/*app.post('/Login', function(req,res){
  res.set("Access-Control-Allow-Headers","Content-Type");
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"'+email+'","password":"'+password+'"}';

  clienteMLabRaiz = requestjson.createClient(urlRaizMlab + "/Usuarios?" + apiKey + "&" + query);
  console.log(urlRaizMlab + "/Usuarios?" + apiKey + "&" + query);
  //res.status(200).send('Usuario logado');

  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length == 1){ //login  ok
        res.status(200).send('Usuario logado');
      } else{
        res.status(404).send('Usuario no encontrado, registrese');
       }
    }
  })
})*/

app.post('/Login', function(req,res){

  collection = db.collection("Usuarios");
  //Valida si existe el usuario en BD
    collection.findOne({email:req.body.email}, function(err, item) {
    if(err){
      console.log("Sin acceso a BD.");
      res.status(400).send("Sin acceso a tabla de usuarios");
    } else{
        if (item == null ) {
          console.log("EL usuario no existe");
          return res.status(201).send("noUsuario");
        } else{
          //Valida el acceso
          collection.findOne({email:req.body.email, password:req.body.password}, function(err2, item) {
            if(err2){
              console.log("Sin acceso a BD.");
              return res.status(400).send("Sin acceso a tabla de usuarios");
            } else{
              if (item == null ) {
                console.log("Contraseña incorrecta");
                return res.status(202).send("noUsuario");
              }else{
                console.log("Usuario Logado");
                return  res.status(200).send('logado');
              }
            }
          })
        }
    }
  })
})


app.post('/Registro', function(req, res) {
        var email = req.body.email;
        var password = req.body.password;
        var nombre = req.body.nombre;
        var apellido = req.body.apellido;

        collection = db.collection("Clientes");
        //Graba en la tabla de Clientes
        collection.insert([{"idcliente" : 999, "nombre" : nombre ,"apellido": apellido,"email" : email,"estatus" : true}], function(err, doc) {
          console.log("Resultado Insert Registro: " + doc);
          //Valida que se grabo para guardar la contraseña en Usuarios
          collection.findOne({email:req.body.email}, function(err2, item) {
            if(err2) {
              console.log("Sin acceso a BD.");
              res.status(400).send("Registro .. noBD");
            }
            else {
              if (item == null ) {
                console.log("No se ha podido registrar el usuario en Clientes");
                return res.status(400).send("Registro fallido");
              } else {
                //Graba en la tabla de Usuarios
                collection = db.collection("Usuarios");
                collection.insert([{"email" : email,"password": password,"estatus" : true}], function(err3, doc) {
                  if(err3){
                    console.log("Problema en el registrao del Usuario.");
                    res.status(400).send("Registro .. noBD");
                  } else{
                    console.log(" Usuario " + item.nombre + " (" + item._id + ") creado correctamente.");
                    //alert("Usuario " + item.nombre + " (" + item._id + ") creado correctamente.");
                    return res.status(200).send("okINSERT");
                  }
              })
             }
           }
        })
      })
}) // cierra POST registro

app.post('/Pedido', function(req, res) {
    //collection = db.collection("Pedidos");
    //Graba en la tabla de Pedidos
    var piezas = Number(0);
    //console.log(req.body);
     db.collection("Pedidos").insertMany([req.body], function(err, doc) {
      if(err){
        console.log("Problema en el registrao del Usuario.");
        res.status(400).send("Registro .. noBD");
      } else{
        //Restamos la existencia
            //console.log('Registros' , req.body.articulos.length)
          for (var i = 0; i <  req.body.articulos.length; i++) {
              //Busca el producto y lo actualiza
              piezas = Number.parseFloat(req.body.articulos[i].piezas)
              var filter = { idProducto: req.body.articulos[i].idProducto};
              var update = { $inc: {unidades_stock: piezas*(-1), unidades_ordenadas: piezas}};
              db.collection("Productos").updateOne(filter, update, function(err2, item) {
                                                        if(err2){
                                                          console.log('Mensaje de Error de peddo', err2);
                                                          return res.status(400).send("Registro .. noBD");
                                                        }
                                                      })
           }
           console.log("Pedido Registrado creado correctamente.");
           return res.status(200).send("okPedido");
      }
    })
  }) // cierra POST pedido

  /* Consigue el historico de pedidos de un usuario*/
  app.get('/Pedido/:idusuario', function(req, res) {
     console.log("Consulta el historico de pedidos de un usuario." + req.params.idusuario);
     db.collection('Pedidos').find({idCliente: req.params.idusuario}).toArray(function(err, result) {
         if(err) {
           console.log(err)
           return res.status(400).send("Problema al consultar los Pedidos");
         }else{
           //console.log(result);
           res.send(result)
         }
     })
    }) // cierra GET pedidos
